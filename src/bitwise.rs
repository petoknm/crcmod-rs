use crate::reversible::Reversible;
use crate::Params;

pub struct BitwiseDigestor<'a, T> {
    params: &'a Params<T>,
    crc: T,
}

impl<'a, T: Reversible> BitwiseDigestor<'a, T> {
    pub fn new(params: &'a Params<T>) -> Self {
        Self {
            params,
            crc: params.init,
        }
    }

    pub fn digest(&mut self, data: &[u8]) {
        for &byte in data {
            // TODO: masking if width is not a power of 2
            let byte = match self.params.refin {
                true => byte.reverse(),
                false => byte,
            };
            self.crc = self.crc ^ T::from(byte).unwrap();
            for _ in 0..8 {
                self.crc = match (self.crc & T::one()) == T::one() {
                    true => (self.crc >> 1) ^ self.params.poly,
                    false => self.crc >> 1,
                };
            }
        }
    }

    // pub fn digest_bit(&mut self, bit: u8) {
    //     // TODO: masking if width is not a power of 2
    //     let bit = T::from(bit).unwrap();
    //     self.crc = match ((self.crc ^ bit) & T::one()) == T::one() {
    //         true => (self.crc >> 1) ^ self.params.poly,
    //         false => self.crc >> 1,
    //     };
    // }

    pub fn finalize(self) -> T {
        let crc = match self.params.refout {
            true => self.crc.reflect(self.params.width),
            false => self.crc,
        };
        crc ^ self.params.xorout
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::*;

    const CHECK: &[u8] = b"123456789";

    macro_rules! test_params {
        ($ident:ident) => {
            paste::item! {
                #[test]
                fn [< $ident:lower _check >]() {
                    let mut dig = BitwiseDigestor::new(&$ident);
                    dig.digest(CHECK);
                    assert_eq!(dig.finalize(), $ident.test.check);
                }
            }
            paste::item! {
                #[test]
                fn [< $ident:lower _codewords >]() {
                    for cw in $ident.test.codewords {
                        let mut dig = BitwiseDigestor::new(&$ident);
                        dig.digest(cw);
                        assert_eq!(dig.finalize() ^ $ident.xorout, $ident.residue);
                    }
                }
            }
        };
    }

    test_params!(CRC8_DARC);
    test_params!(CRC8_SMBUS);
    test_params!(CRC8_WCDMA);
    test_params!(CRC32_BZIP2);
}
