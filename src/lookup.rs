use crate::reversible::Reversible;
use crate::Params;

use num_traits::FromPrimitive;

pub struct LookupTable<T>([T; 256]);

impl<T: Reversible + FromPrimitive> LookupTable<T> {
    pub fn new(params: &Params<T>) -> Self {
        let mut table = [T::zero(); 256];
        let bit_mask = T::from(1u8).unwrap();
        for i in 0..256 {
            let byte = i as u8;
            let byte = match params.refin {
                true => byte.reverse(),
                false => byte,
            };
            let mut res = T::from(byte).unwrap();
            for _ in 0..8 {
                res = match (res & bit_mask).to_u8().unwrap() == 1 {
                    true => (res >> 1) ^ params.poly,
                    false => res >> 1,
                };
            }
            table[i] = res;
        }
        Self(table)
    }
}

pub struct LookupDigestor<'a, T> {
    params: &'a Params<T>,
    table: &'a LookupTable<T>,
    crc: T,
}

impl<'a, T: Reversible + FromPrimitive> LookupDigestor<'a, T> {
    pub fn new(params: &'a Params<T>, table: &'a LookupTable<T>) -> Self {
        Self {
            params,
            table,
            crc: params.init,
        }
    }

    pub fn digest(&mut self, data: &[u8]) {
        let byte_mask = T::from(0xffu8).unwrap();
        let n = std::mem::size_of_val(&byte_mask);
        for &byte in data {
            let index = (self.crc & byte_mask).to_u8().unwrap() ^ byte;
            let val = self.table.0[index as usize];
            self.crc = if n == 1 { val } else { (self.crc >> 8) ^ val }
        }
    }

    pub fn finalize(self) -> T {
        let crc = match self.params.refout {
            true => self.crc.reflect(self.params.width),
            false => self.crc,
        };
        crc ^ self.params.xorout
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::*;

    const CHECK: &[u8] = b"123456789";

    macro_rules! test_params {
        ($ident:ident) => {
            paste::item! {
                #[test]
                fn [< $ident:lower _check >]() {
                    let table = LookupTable::new(&$ident);
                    let mut dig = LookupDigestor::new(&$ident, &table);
                    dig.digest(CHECK);
                    assert_eq!(dig.finalize(), $ident.test.check);
                }
            }
            paste::item! {
                #[test]
                fn [< $ident:lower _codewords >]() {
                    let table = LookupTable::new(&$ident);
                    for cw in $ident.test.codewords {
                        let mut dig = LookupDigestor::new(&$ident, &table);
                        dig.digest(cw);
                        assert_eq!(dig.finalize() ^ $ident.xorout, $ident.residue);
                    }
                }
            }
        };
    }

    test_params!(CRC8_DARC);
    test_params!(CRC8_SMBUS);
    test_params!(CRC8_WCDMA);
    test_params!(CRC32_BZIP2);
}
