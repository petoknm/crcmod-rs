use num_traits::PrimInt;

pub trait Reversible: PrimInt {
    fn reverse(self) -> Self;
    fn reflect(self, width: u8) -> Self;
}

macro_rules! reversible {
    ($ty:ty) => {
        #[allow(overflowing_literals)]
        impl Reversible for $ty {
            fn reverse(self) -> Self {
                let mut val = self;
                // Swap odd and even bits
                val = ((val >> 1) & (0x5555555555555555 as $ty))
                    | ((val & (0x5555555555555555 as $ty)) << 1);
                // Swap consecutive pairs
                val = ((val >> 2) & (0x3333333333333333 as $ty))
                    | ((val & (0x3333333333333333 as $ty)) << 2);
                // Swap nibbles
                val = ((val >> 4) & (0x0F0F0F0F0F0F0F0F as $ty))
                    | ((val & (0x0F0F0F0F0F0F0F0F as $ty)) << 4);
                val.swap_bytes()
            }
            fn reflect(self, width: u8) -> Self {
                let n = 8 * std::mem::size_of::<$ty>() as u8;
                let rshift = (n - width) as usize;
                self.reverse() >> rshift
            }
        }
    };
}

reversible!(u8);
reversible!(u16);
reversible!(u32);
reversible!(u64);
