use crcmod::{BitwiseDigestor, LookupDigestor, LookupTable, CRC8_DARC};
use criterion::{black_box, criterion_group, criterion_main, Criterion, Throughput};

const DATA: &[u8] = &[0x55; 64 * 1024];

pub fn bench(c: &mut Criterion) {
    let mut g = c.benchmark_group("CRC8_DARC");
    g.throughput(Throughput::Bytes(DATA.len() as u64));
    g.bench_function("bitwise", |b| {
        let mut dig = BitwiseDigestor::new(&CRC8_DARC);
        b.iter(|| black_box(dig.digest(DATA)));
        dig.finalize();
    });
    g.bench_function("lookup", |b| {
        let table = LookupTable::new(&CRC8_DARC);
        let mut dig = LookupDigestor::new(&CRC8_DARC, &table);
        b.iter(|| black_box(dig.digest(DATA)));
        dig.finalize();
    });
    g.finish();
}

criterion_group!(benches, bench);
criterion_main!(benches);
